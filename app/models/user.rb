class User < ActiveRecord::Base
	has_many :authorizations
	validates :name, :email, presence: true

	def provider
		#Verifique se o provider já existe, então não adicioná-lo duas vezes
		unless authorizations.find_by_provider_and_uid(auth_hash["provider"], auth_hash["uid"])
			Authorization.create :user => self, :provider => auth_hash["provider"], :uid => auth_hash["uid"]
		end
	end
end
