class SessionsController < ApplicationController
  def new
  end

  def create
  	auth_hash = request.env['omniatuh.auth']

  	if session[:user_id]
  		# Significa que o nosso usuário está conectado. Adicionar a autorização para o usuário
  		User.find(session[:user_id]).add_provider(auth_hash)
  	  render :text => "Agora você pode fazer login usando #{auth_hash["provider"].capitalize} também!"
    else
      # Logado
      auth = Authorization.find_or_create(auth_hash)
 
      # Cira a sessão
      session[:user_id] = auth.user.id
 
      render :text => "Bem vindo #{auth.user.name}!"
    end
  end

  def failure
  	render :text => "Desculpe, mas você não tem permissão de acesso para nossa app!"
  end

  def destroy
  	session[:user_id] = nil
  	render text: "Você foi deslogado!"
  end
end
