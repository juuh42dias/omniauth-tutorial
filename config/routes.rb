Rails.application.routes.draw do
  get 'sessions/new'

  get 'sessions/create'

  get 'sessions/failure'

  root to: '/login', :to => 'sessions#new', :as => :login
  post '/auth/:provider/callback', :to => 'sessions#create'
  post '/auth/failure', :to => 'sessions#failure'

  get '/logout', :to => 'sessions#destroy'
end
